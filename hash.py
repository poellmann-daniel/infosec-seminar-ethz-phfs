from PIL import Image
import imagehash
import sys

image = Image.open(sys.argv[1])
hash_size = int(sys.argv[2])

phash = imagehash.phash(image, hash_size=hash_size)
ahash = imagehash.average_hash(image, hash_size=hash_size)
dhash = imagehash.dhash(image, hash_size=hash_size)
whash = imagehash.whash(image, hash_size=hash_size)

print(f"pHash: {phash}")
print(f"aHash: {ahash}")
print(f"dHash: {dhash}")
print(f"wHash: {whash}")

