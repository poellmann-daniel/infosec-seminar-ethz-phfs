import math
import random

from PIL import Image
import imagehash
import sys
from PIL import Image
import numpy as np

hash_size = 16

image = Image.open(sys.argv[1])
ahash1 = imagehash.average_hash(image, hash_size=hash_size)
print(f"ahash = {ahash1}")
image = Image.open(sys.argv[2])
ahash2 = imagehash.average_hash(image, hash_size=hash_size)
print(f"ahash = {ahash2}")
print(f"Distance: {ahash2 - ahash1}")

image = Image.open(sys.argv[1])
dhash1 = imagehash.dhash(image, hash_size=hash_size)
print(f"dhash = {dhash1}")
image = Image.open(sys.argv[2])
dhash2 = imagehash.dhash(image, hash_size=hash_size)
print(f"dhash = {dhash2}")
print(f"Distance: {dhash2 - dhash1}")

image = Image.open(sys.argv[1])
dhash1 = imagehash.phash(image, hash_size=hash_size)
print(f"phash = {dhash1}")
image = Image.open(sys.argv[2])
dhash2 = imagehash.phash(image, hash_size=hash_size)
print(f"phash = {dhash2}")
print(f"Distance: {dhash2 - dhash1}")

