import math

from PIL import Image
import imagehash
import sys
from PIL import Image
import numpy as np


def row_to_pixels(row):
    black_pixel = (0, 0, 0)
    white_pixel = (255, 255, 255)

    pixels = []
    for bit in row:
        if int(bit) == 1:
            pixels.append(white_pixel)
        else:
            pixels.append(black_pixel)
    return pixels


def print_ascii(pixels):
    for row in pixels:
        print(row)


def find_min_max_values_in_sequence(bits, base_value):
    min = base_value
    max = base_value
    current = base_value
    for bit in bits:
        if int(bit) == 0:
            current = current - 1
        else:
            current = current + 1

        if current > max:
            max = current

        if current < min:
            min = current

    return min, max


def reverse(dhash, step_size=5, base_value=128):
    print(dhash)

    hash = bytes.fromhex(str(dhash))

    bits = binary_string = "{:08b}".format(int(hash.hex(), 16))

    print(f"len(bits)={len(bits)}")
    image_size_width = hash_size
    image_size_height = hash_size
    print(f"Recovering Image of Size: {image_size_width}x{image_size_height} (width x height)")

    pixels = []
    for i in range(0, len(bits), hash_size):
        pixels.append(bits[i:i + hash_size])

    pixels = pixels[:-1]

    pixel_image = []
    for row_number, row in enumerate(pixels):
        pixel_row = []
        min, max = find_min_max_values_in_sequence(row, base_value)
        min_prev, max_prev = find_min_max_values_in_sequence(row, base_value)
        average_of_previous_row = int(0.5 * (min_prev + max_prev))
        current = int(0.5 * (min + max)) # + int(average_of_previous_row * 0.5)

        step = step_size
        for bit in row:
            if int(bit) == 1:
                current = current + step
            else:
                current = current - step

            pixel_row.append((current, current, current))
        pixel_image.append(pixel_row)

    # print_ascii(pixels)
    # print(pixel_image)

    array = np.array(pixel_image, dtype=np.uint8)

    new_image = Image.fromarray(array)
    new_image.save(f"dhash_recovery/dhash_recovered_{step_size}_{base_value}.png")


image = Image.open(sys.argv[1])
hash_size = int(sys.argv[2])
dhash = imagehash.dhash(image, hash_size=hash_size)

for step_size in range(0, 20):
    for base_value in range(64, 192, 10):
        reverse(dhash, step_size, base_value)



