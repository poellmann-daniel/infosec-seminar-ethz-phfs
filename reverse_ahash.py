import math

from PIL import Image
import imagehash
import sys
from PIL import Image
import numpy as np


def row_to_pixels(row):
    black_pixel = (0, 0, 0)
    white_pixel = (255, 255, 255)

    pixels = []
    for bit in row:
        if int(bit) == 1:
            pixels.append(white_pixel)
        else:
            pixels.append(black_pixel)
    return pixels


def print_ascii(pixels):
    for row in pixels:
        print(row)


image = Image.open(sys.argv[1])
hash_size = int(sys.argv[2])

ahash = imagehash.average_hash(image, hash_size=hash_size)
print(f"ahash = {ahash}")
hash = bytes.fromhex(str(ahash))

bits = binary_string = "{:08b}".format(int(hash.hex(), 16))

image_size = int(math.sqrt(len(bits)))
print(f"Recovering Image of Size: {image_size}x{image_size}")

pixels = []
for i in range(0, len(bits), image_size):
    pixels.append(bits[i:i+image_size])

# print_ascii(pixels)

pixels = list(map(lambda row: row_to_pixels(row), pixels))
# print(pixels)

array = np.array(pixels, dtype=np.uint8)

new_image = Image.fromarray(array)
new_image.save('ahash_recovered.png')
