import math
import random

from PIL import Image
import imagehash
import sys
from PIL import Image
import numpy as np

width = 640
height = 480

def row_to_pixels(row):
    black_pixel = (0, 0, 0)
    white_pixel = (255, 255, 255)

    pixels = []
    for bit in row:
        if int(bit) == 1:
            pixels.append(white_pixel)
        else:
            pixels.append(black_pixel)
    return pixels

pixels = []
for h in range(height):
   pixels.append(random.choices([0, 1], k=width))


pixels = list(map(lambda row: row_to_pixels(row), pixels))
# print(pixels)

array = np.array(pixels, dtype=np.uint8)

new_image = Image.fromarray(array)
new_image.save('noise.png')