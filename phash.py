# Adapted from: https://raw.githubusercontent.com/1Presidio/phash/master/main.py

import numpy as np
import os, sys
from PIL import Image


def reduceimg(img):
    outfile = os.path.splitext(img)[0] + "_" + ".jpeg"
    try:
        # reduced image dimensions
        size = (8, 8)
        img = Image.open(img)
        img = img.resize(size, Image.ANTIALIAS)
        return img
    except IOError:
        raise Exception("Bad File")


# convert to grey scale
def greyscale(img):
    img = img.convert('1')
    return img


# get average greyscale color
def average_colors(img):
    pixelWeight = list(img.getdata())
    listLen = len(pixelWeight)
    totalsum = 0
    counter = 0
    for i in range(listLen):
        totalsum += pixelWeight[i]
        counter += 1
    averageVal = totalsum / counter
    return averageVal


# compares img average to every pixel
# gets img hash value
def compare_bits(img, imgAvg):
    pixelWeight = list(img.getdata())
    listLen = len(pixelWeight)
    assert (listLen == 64)
    bitRes = ""
    for i in range(listLen):
        greyscale = rgb2grey(pixelWeight[i])
        if greyscale > imgAvg:
            bitRes += "1"
        else:
            bitRes += "0"
    return bitRes


# greyscale image formula
# convert tuple to greyscaled pixel
def rgb2grey(rgbTuple):
    red = rgbTuple[0]
    green = rgbTuple[1]
    blue = rgbTuple[2]
    greyscale = 0.299 * red + 0.587 * green + 0.114 * blue
    return greyscale


# https://en.wikipedia.org/wiki/Hamming_distance
def hammingDifference(bitNum1, bitNum2):
    result = 0
    for index in range(len(bitNum1)):
        if (bitNum2[index] != bitNum1[index]):
            result += 1
    return result


def main():
    # reduces image, greyscales
    # averages and then hashes image
    img1 = sys.argv[1]
    img1 = reduceimg(img1)
    greyImg1 = greyscale(img1)
    imgAvg1 = average_colors(greyImg1)
    bitHash1 = compare_bits(img1, imgAvg1)

    # print results
    print()
    print("phash: ", (hex(eval(("0b" + bitHash1)))[2:]))

    return


if __name__ == "__main__":
    main()
